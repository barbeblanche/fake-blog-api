import { todo } from './todos/todos.js'

import { comment } from './comments/comments.js'

import { post } from './posts/posts.js'

import { user } from './users/users.js'

export const services = (app) => {
  app.configure(todo)

  app.configure(comment)

  app.configure(post)

  app.configure(user)

  // All services will be registered here
}
