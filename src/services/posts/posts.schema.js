// // For more information about this file see https://dove.feathersjs.com/guides/cli/service.schemas.html
import { resolve, virtual } from '@feathersjs/schema'
import { Type, getValidator, querySyntax } from '@feathersjs/typebox'
import { dataValidator, queryValidator } from '../../validators.js'

// Main data model schema
export const postSchema = Type.Object(
  {
    id: Type.String({ format: 'uuid' }),
    content: Type.String(),
    title: Type.String(),
    user_id: Type.String({ format: 'uuid' }),
    created_at: Type.String({ format: 'date-time' }),
    updated_at: Type.String({ format: 'date-time' }),
  },
  { $id: 'Post', additionalProperties: false }
)
export const postValidator = getValidator(postSchema, dataValidator)
export const postResolver = resolve({
  user : virtual(async (post, context) => {
    return await context.app.service('users').get(post.user_id);
  })
})

export const postExternalResolver = resolve({})

// Schema for creating new entries
export const postDataSchema = Type.Pick(postSchema, ['content', 'title'], {
  $id: 'PostData'
})
export const postDataValidator = getValidator(postDataSchema, dataValidator)
export const postDataResolver = resolve({
  user_id : async (user_id, obj, context) => context.params.user.id,
})

// Schema for updating existing entries
export const postPatchSchema = Type.Partial(postSchema, {
  $id: 'PostPatch'
})
export const postPatchValidator = getValidator(postPatchSchema, dataValidator)
export const postPatchResolver = resolve({})

// Schema for allowed query properties
export const postQueryProperties = Type.Pick(postSchema, ['id', 'title', 'user_id', 'created_at'])
export const postQuerySchema = Type.Intersect(
  [
    querySyntax(postQueryProperties),
    // Add additional query properties here
    Type.Object({}, { additionalProperties: false })
  ],
  { additionalProperties: false }
)
export const postQueryValidator = getValidator(postQuerySchema, queryValidator)
export const postQueryResolver = resolve({})
