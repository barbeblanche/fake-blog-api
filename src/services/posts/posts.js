// For more information about this file see https://dove.feathersjs.com/guides/cli/service.html
import { authenticate } from '@feathersjs/authentication'

import { hooks as schemaHooks } from '@feathersjs/schema'
import {
  postDataValidator,
  postPatchValidator,
  postQueryValidator,
  postResolver,
  postExternalResolver,
  postDataResolver,
  postPatchResolver,
  postQueryResolver
} from './posts.schema.js'
import { PostService, getOptions } from './posts.class.js'

export const postPath = 'posts'
export const postMethods = ['find', 'get', 'create', 'patch', 'remove']

export * from './posts.class.js'
export * from './posts.schema.js'

// A configure function that registers the service and its hooks via `app.configure`
export const post = (app) => {
  // Register our service on the Feathers application
  app.use(postPath, new PostService(getOptions(app)), {
    // A list of all methods this service exposes externally
    methods: postMethods,
    // You can add additional custom events to be sent to clients here
    events: []
  })
  // Initialize hooks
  app.service(postPath).hooks({
    around: {
      all: [
        authenticate('jwt'),
        schemaHooks.resolveExternal(postExternalResolver),
        schemaHooks.resolveResult(postResolver)
      ]
    },
    before: {
      all: [schemaHooks.validateQuery(postQueryValidator), schemaHooks.resolveQuery(postQueryResolver)],
      find: [],
      get: [],
      create: [schemaHooks.validateData(postDataValidator), schemaHooks.resolveData(postDataResolver)],
      patch: [schemaHooks.validateData(postPatchValidator), schemaHooks.resolveData(postPatchResolver)],
      remove: []
    },
    after: {
      all: []
    },
    error: {
      all: []
    }
  })
}
