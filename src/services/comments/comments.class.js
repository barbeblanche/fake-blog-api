import { KnexService } from '@feathersjs/knex'

// By default calls the standard Knex adapter service methods but can be customized with your own functionality.
export class CommentService extends KnexService {

  setup(app) {
    this.app = app;
  }

  async create(data, params) {

    const sqliteClient = this.app.get('sqliteClient');

    let createUser = await sqliteClient.table('comments').insert(data).returning('id')

    return await sqliteClient.table('comments').where('id', createUser[0].id).first();
  }
}

export const getOptions = (app) => {
  return {
    paginate: false,
    Model: app.get('sqliteClient'),
    name: 'comments'
  }
}
