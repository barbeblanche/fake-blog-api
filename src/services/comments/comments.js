// For more information about this file see https://dove.feathersjs.com/guides/cli/service.html
import { authenticate } from '@feathersjs/authentication'

import { hooks as schemaHooks } from '@feathersjs/schema'
import {
  commentDataValidator,
  commentPatchValidator,
  commentQueryValidator,
  commentResolver,
  commentExternalResolver,
  commentDataResolver,
  commentPatchResolver,
  commentQueryResolver
} from './comments.schema.js'
import { CommentService, getOptions } from './comments.class.js'

export const commentPath = 'comments'
export const commentMethods = ['find', 'get', 'create', 'patch', 'remove']

export * from './comments.class.js'
export * from './comments.schema.js'

// A configure function that registers the service and its hooks via `app.configure`
export const comment = (app) => {
  // Register our service on the Feathers application
  app.use(commentPath, new CommentService(getOptions(app)), {
    // A list of all methods this service exposes externally
    methods: commentMethods,
    // You can add additional custom events to be sent to clients here
    events: []
  })
  // Initialize hooks
  app.service(commentPath).hooks({
    around: {
      all: [
        authenticate('jwt'),
        schemaHooks.resolveExternal(commentExternalResolver),
        schemaHooks.resolveResult(commentResolver)
      ]
    },
    before: {
      all: [schemaHooks.validateQuery(commentQueryValidator), schemaHooks.resolveQuery(commentQueryResolver)],
      find: [],
      get: [],
      create: [schemaHooks.validateData(commentDataValidator), schemaHooks.resolveData(commentDataResolver)],
      patch: [schemaHooks.validateData(commentPatchValidator), schemaHooks.resolveData(commentPatchResolver)],
      remove: []
    },
    after: {
      all: []
    },
    error: {
      all: []
    }
  })
}
