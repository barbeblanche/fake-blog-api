// // For more information about this file see https://dove.feathersjs.com/guides/cli/service.schemas.html
import { resolve, virtual } from '@feathersjs/schema'
import { Type, getValidator, querySyntax } from '@feathersjs/typebox'
import { dataValidator, queryValidator } from '../../validators.js'
import { BadRequest } from '@feathersjs/errors'

// Main data model schema
export const commentSchema = Type.Object(
  {
    id: Type.String({ format: 'uuid' }),
    content: Type.String(),
    user_id: Type.String({ format: 'uuid' }),
    post_id: Type.String({ format: 'uuid' }),
    created_at: Type.String({ format: 'date-time' }),
    updated_at: Type.String({ format: 'date-time' }),
  },
  { $id: 'Comment', additionalProperties: false }
)
export const commentValidator = getValidator(commentSchema, dataValidator)
export const commentResolver = resolve({
  user : virtual (async (comment, context) => {
    return await context.app.service('users').get(comment.user_id);
  })
})

export const commentExternalResolver = resolve({})

// Schema for creating new entries
export const commentDataSchema = Type.Pick(commentSchema, ['content', 'post_id'], {
  $id: 'CommentData'
})
export const commentDataValidator = getValidator(commentDataSchema, dataValidator)
export const commentDataResolver = resolve({
  user_id : async (user_id, comment, context) => context.params.user.id,
  post_id : async (post_id, comment, context) => {
    let existPost = await context.app.service('posts').find({query : {id: post_id, $limit: 1}});
    if(existPost.length < 1) {
      throw new BadRequest('This post not exist')
    }
    return post_id
  }
})

// Schema for updating existing entries
export const commentPatchSchema = Type.Partial(commentSchema, {
  $id: 'CommentPatch'
})
export const commentPatchValidator = getValidator(commentPatchSchema, dataValidator)
export const commentPatchResolver = resolve({})

// Schema for allowed query properties
export const commentQueryProperties = Type.Pick(commentSchema, ['id', 'user_id', 'post_id'])
export const commentQuerySchema = Type.Intersect(
  [
    querySyntax(commentQueryProperties),
    // Add additional query properties here
    Type.Object({}, { additionalProperties: false })
  ],
  { additionalProperties: false }
)
export const commentQueryValidator = getValidator(commentQuerySchema, queryValidator)
export const commentQueryResolver = resolve({})
