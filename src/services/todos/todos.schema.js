// // For more information about this file see https://dove.feathersjs.com/guides/cli/service.schemas.html
import { resolve } from '@feathersjs/schema'
import { Type, getValidator, querySyntax } from '@feathersjs/typebox'
import { dataValidator, queryValidator } from '../../validators.js'

// Main data model schema
export const todoSchema = Type.Object(
  {
    id: Type.String({ format: 'uuid' }),
    description: Type.String(),
    title: Type.String(),
    user_id: Type.String({ format: 'uuid' }),
    priority: Type.Enum({low : 'low', medium: 'medium', high: 'high'}),
    begined_at: Type.Optional(Type.String()),
    finished_at: Type.Optional(Type.String()),
    deadline_at: Type.Optional(Type.String()),
    created_at: Type.String({ format: 'date-time' }),
    updated_at: Type.String({ format: 'date-time' }),
  },
  { $id: 'Todo', additionalProperties: false }
)
export const todoValidator = getValidator(todoSchema, dataValidator)
export const todoResolver = resolve({})

export const todoExternalResolver = resolve({})

// Schema for creating new entries
export const todoDataSchema = Type.Pick(todoSchema, ['title', 'description', 'priority', 'deadline_at', 'begined_at', 'finished_at'], {
  $id: 'TodoData'
})
export const todoDataValidator = getValidator(todoDataSchema, dataValidator)
export const todoDataResolver = resolve({
  user_id : async (user_id, obj, context) => context.params.user.id,
})

// Schema for updating existing entries
export const todoPatchSchema = Type.Partial(todoSchema, {
  $id: 'TodoPatch'
})
export const todoPatchValidator = getValidator(todoPatchSchema, dataValidator)
export const todoPatchResolver = resolve({})

// Schema for allowed query properties
export const todoQueryProperties = Type.Pick(todoSchema, ['id', 'priority', 'deadline_at', 'begined_at', 'finished_at', 'user_id', 'created_at'])
export const todoQuerySchema = Type.Intersect(
  [
    querySyntax(todoQueryProperties),
    // Add additional query properties here
    Type.Object({}, { additionalProperties: false })
  ],
  { additionalProperties: false }
)
export const todoQueryValidator = getValidator(todoQuerySchema, queryValidator)
export const todoQueryResolver = resolve({
  user_id: async (value, user, context) => {
    if (context.params.user) {
      return context.params.user.id
    }
    return value
  }
})
