# blog

> 

## Getting Started

1. Make sure you have [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed.
2. Install your dependencies

    ```
    cd path/to/feathers-fake-blog
    npm install
    ```

3. Start your app

    ```
    npm run migrate # Run migrations to set up the database
    npm start
    ```

4. Navigate to the [Documentation](https://documenter.getpostman.com/view/3550891/2s8YzS13a7) to see how to use the endpoints


## About

This project uses [Feathers](http://feathersjs.com). An open source framework for building APIs and real-time applications.